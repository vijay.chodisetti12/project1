package Assignments4;
/* Task-4*/
/*Declaration, Initialization, and Usage
Create a program that declares an array of integers,
initializes it with consecutive numbers, and prints the array in reverse order.
 */
public class ArrayReverseorder2 {
            public static void main(String[] args) {
                // Declare an array of integers
                int[] numbers = new int[10];

                // Initialize the array
                for (int i = 0; i < numbers.length; i++) {
                    numbers[i] = i + 1; // Consecutive numbers starting from 1
                }

                // Print the array in reverse order
                System.out.println("Array in reverse order:");
                for (int i = numbers.length - 1; i >= 0; i--) {
                    System.out.print(numbers[i] + " ");
                }
            }
        }