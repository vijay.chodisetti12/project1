package Assignments4;
/* Task - 3
Write a Java program that reads an integer and prints whether it is a prime number
using a for loop and if statements.
 */

import java.util.Scanner;
public class PrimeNumbers1 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a Number :");
        int number = sc.nextInt();
        sc.close();

        boolean isPrime = true;

        if (number <= 1)
        {
            isPrime = false;
        } else {
            for(int i =2; i <= Math.sqrt(number);i++) {
                if(number % i == 0) {
                    isPrime = false;
                    break;
                }
            }
        }
        if (isPrime) {
            System.out.println(number + " is a prime number.");
        } else {
            System.out.println(number + " is not a prime number.");
        }
    }

}
