package Day4_assignments;

/* Write a recursive function named SumArray that calculates and returns the sum of
elements in an array, demonstrate with example. */
public class SumArray4 {
    public static int SumArray(int arr[], int i) {
        if (i < 0 || i >= arr.length) { //condition
            return 0;
        }
        return arr[i] + SumArray(arr, i + 1);
    }

    public static void main(String args[]) {
        int[] arr = {10, 20, 30, 45, 55, 60, 70, 80, 90, 10};
        int sum = SumArray(arr, 0);
        System.out.println("The Sum of Elements in Array is :" + sum);
    }
}
