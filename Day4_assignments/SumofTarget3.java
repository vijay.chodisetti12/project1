package Day4_assignments;

import java.util.Scanner;
import java.util.Arrays;

public class SumofTarget3 {
    public static void main(String[] args) {
        int[] numbers = {3, 10, 12, 14};
        int target;

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Target: ");
        target = sc.nextInt();
        Arrays.sort(numbers);

        int left = 0;
        int right = numbers.length - 1;

        // To traverse the array
        while (left < right) {
            int sum = numbers[left] + numbers[right];
            if (sum == target) {
                System.out.println("Two Numbers that add up to " + target + " are: " + numbers[left] + " , " + numbers[right]);
                return;
            } else if (sum < target) {
                left++;
            } else {
                right--;
            }
        }

        // If No solution Found
        System.out.println("No solution found");
    }
}
