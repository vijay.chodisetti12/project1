package Day5_assignments;
/* Task 7:
* Implement a function called BruteForceSort that sorts an array using the
* brute force approach. Use this function to sort an array created with InitializeArray */

import java.util.Arrays;
public class BruteForceSort1 {
    public static void bruteForceSort(int[]arr) {
        int n = arr.length;

        for (int i = 0; i < arr.length; i++) {
            int minIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }
            int temp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = temp;
        }
    }

    public static void main(String[] args) {
        int[] myArray = {50,70,2,9,80}; // Example array

        System.out.println("Array before sorting: " + Arrays.toString(myArray));

        bruteForceSort(myArray);

        System.out.println("Array after sorting: " + Arrays.toString(myArray));
    }
}