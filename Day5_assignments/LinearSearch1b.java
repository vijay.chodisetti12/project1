package Day5_assignments;
/*b) Write a function named PerformLinearSearch that searches for a
 specific element in an array and returns the index of the element if
 found or -1 if not found.
 */

import java.util.Scanner;
public class LinearSearch1b {

    public static int performLinearSearch(int[] arr, int target) {
        // Traverse through the array
        for (int i = 0; i < arr.length; i++) {

            if (arr[i] == target) {
                return i;
            }
        }
        // If the target is not found in the array, return -1
        return -1;
    }

    public static void main(String[] args) {
        // Scanner to take user input
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the number of elements in the array: ");
        int n = sc.nextInt();
        // Create an array of size n
        int[] array = new int[n];

        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < n; i++) {
            array[i] = sc.nextInt();
        }


        System.out.print("Enter the target element to search for: ");
        int target = sc.nextInt();


        int index = performLinearSearch(array, target);

        if (index != -1) {
            System.out.println("Element " + target + " found at index: " + index);
        } else {
            System.out.println("Element " + target + " not found in the array.");
        }
        sc.close();
    }
}


