package Day5_assignments;
/*Create a recursive function to find the nth element of a Fibonacci sequence
and store the first n elements in an array.
 */
import java.util.Scanner;

public class RecursiveFibbonacci2b {

        public static int fibonacci(int n) {
            if (n <= 1) {
                return n;
            } else {
                return fibonacci(n - 1) + fibonacci(n - 2);
            }
        }
        public static int[] generateFibonacciArray(int n) {
            int[] fibonacciArray = new int[n];
            for (int i = 0; i < n; i++) {
                fibonacciArray[i] = fibonacci(i);
            }
            return fibonacciArray;
        }

        public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);

            System.out.print("Enter the number of Fibonacci elements to generate: ");
            int n = sc.nextInt();

            int[] fibonacciSequence = generateFibonacciArray(n);

            System.out.print("The first " + n + " Fibonacci numbers are: ");
            for (int i = 0; i < n; i++) {
                System.out.print(fibonacciSequence[i] + " ");
            }
            System.out.println();
            sc.close();
        }
    }


