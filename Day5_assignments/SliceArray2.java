package Day5_assignments;
/*Implement a method SliceArray that takes an array, a starting index, and an end index, then returns a new
array containing the elements from the start to the end index.
 */
import java.util.Arrays;

/* Implement a method SliceArray that takes an array, a starting index, and an end index, then returns
a new array containing the elements from the start to the end index */
public class SliceArray2 {
    /*Slice of Array */
    public static void display(){
        System.out.println("*********** Slice Array *************");
    }

    public static void sliceArray(int[] element,int start,int end){
        int[] slice=new int[end-start+1];
        for(int i =0; i < slice.length; i++){
            slice[i]=element[start+i];  //slice from position - start
        }
        System.out.println("Sliced Array from "+start+" to "+(end-1)+" is :"+ Arrays.toString(slice));
    }

    public static void main(String[] args){

        display(); //Invoke static Methods

        int[] element={10,14,25,3,30,40,20,15,89,90};
        int start=0,end=9;

        System.out.println("Original Array :"+Arrays.toString(element));
        sliceArray(element,start,end); //Call to sliceArray() method
    }
}
