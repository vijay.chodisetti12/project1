package Day5_assignments;
/*String Operations
Write a method that takes two strings, concatenates them, reverses the result,
 and then extracts the middle substring of the given length. Ensure your method handles edge cases,
 such as an empty string or a substring length larger than the concatenat */
public class StringOperations3 {
        public static String manipulateStrings(String str1, String str2, int substringLength) {
            String concatenatedString = str1 + str2;
            System.out.println("Two Strings Concatenated :"+concatenatedString);

            StringBuilder reversedString = new StringBuilder(concatenatedString).reverse();
            System.out.println("The String Reverse order :"+reversedString);

            if (concatenatedString.isEmpty() || substringLength > reversedString.length()) {
                return "";
            }

            int midStart = (reversedString.length() / 2) - (substringLength / 2);
            int midEnd = midStart + substringLength;
            System.out.println("The String is MidStart : "+midStart);
            System.out.println("The String is MidEnd : "+midEnd);

            return reversedString.substring(midStart, midEnd);
        }

        public static void main(String[] args) {
            String string1 = "hello";
            String string2 = "world";
            int substringLen = 5;
            String middleSubstring = manipulateStrings(string1, string2, substringLen);
            System.out.println(middleSubstring);
        }
    }